package APSA_Basic_Functionality;



import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.regex.Pattern;

import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.Selenium;

public class A_APSA_Login {
	private Selenium selenium;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*firefox", "https://www.eapsa.org/");
		selenium.start();
	}

	@Test
	public void testAPSA_Alt_Login_TC() throws Exception {
		selenium.open("/AM/Template.cfm?Section=Roundpoint_Login&Template=/Security/Login.cfm");
		selenium.type("id=EmailAddress", "GSUBSCRIBER");
		selenium.type("id=Password", "GSUBSCRIBER");
		selenium.click("css=td > input[name=\"LoginButton\"]");
		selenium.waitForPageToLoad("30000");


	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
