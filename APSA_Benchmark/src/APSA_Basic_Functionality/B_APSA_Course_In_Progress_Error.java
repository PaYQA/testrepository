package APSA_Basic_Functionality;

import static org.junit.Assert.*;

import com.thoughtworks.selenium.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;


public class B_APSA_Course_In_Progress_Error {
	private Selenium selenium;
	//private Title pageTitle

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*firefox", "https://www.eapsa.org/");
		selenium.start();
	}
	

	@Test
	public void testAPSA_Alt_Login_TC() throws Exception {
		selenium.open("/AM/Template.cfm?Section=Roundpoint_Login&Template=/Security/Login.cfm");
		selenium.type("id=EmailAddress", "GSUBSCRIBER");
		selenium.type("id=Password", "GSUBSCRIBER");
		selenium.click("css=td > input[name=\"LoginButton\"]");

		selenium.waitForPageToLoad("300000");
		
	
		
		//selenium.click("css=#WelcomePageContainer_customCourse > span");
		
		//This needs to check whether the expected page exam.eapsa.org/reader has loaded, or if the screen has returned to https://www.eapsa.org/AM/Template.cfm?Section=Roundpoint_Login&template=/security/Login.cfm
		//If the expected page loads then the xpath will work
		selenium.click("xpath=//div/a/[@id='WelcomePageContainer_generateCourse'/span='Generate Courses & POC']");
		//If the expected page fails then I need to add something here to catch it
		//If it is caught I need to force the exam.eapsa.org/reader page to load
		
		//if [page is apsa page] then
		//if 
		String pageTitle = selenium.getTitle();
			assertTrue ("Page is Roundpoint Login",pageTitle, pageTitle("APSA | Roundpoint Login"));
		
		//click link, then continue with the test
			{selenium.click("xpath=//div[2]/a/img");
			selenium.waitForPageToLoad("30000");
			selenium.click("xpath=//div[4]/div/p[2]/a");
			selenium.waitForPageToLoad("30000");
			selenium.click("xpath=//tbody/tr/td/p/strong/font/a[@href='http://exam.eapsa.org/reader/']");
			selenium.waitForPageToLoad("30000");
			selenium.click("id=GenerateExamPageContainer_generateCourse");
			selenium.type("css=input.keyword-input", "liver");
			selenium.click("css=div.btn.search-btn > span");
			selenium.click("css=div.start-btn.btn > span");
			selenium.click("css=span.btn.ok-btn > span");}
		//else 
			//if [page is Reader page] continue with the test
			//else
			
				
		//using a While instead of an if
				//While (pageTitle == "APSA | Roundpoint Login)
				//selenium.click("xpath=//div[2]/a/img");
				//selenium.waitForPageToLoad("30000");
				//selenium.click("xpath=//div[4]/div/p[2]/a");
				//selenium.waitForPageToLoad("30000");
				//selenium.click("xpath=//tbody/tr/td/p/strong/font/a[@href='http://exam.eapsa.org/reader/']");
				//selenium.waitForPageToLoad("30000");
				//selenium.click("id=GenerateExamPageContainer_generateCourse");
				//selenium.type("css=input.keyword-input", "liver");
				//selenium.click("css=div.btn.search-btn > span");
				//selenium.click("css=div.start-btn.btn > span");
				//selenium.click("css=span.btn.ok-btn > span");}
				
				//else
				
				//selenium.click("id=GenerateExamPageContainer_generateCourse");
				//selenium.type("css=input.keyword-input", "liver");
				//selenium.click("css=div.btn.search-btn > span");
				//selenium.click("css=div.start-btn.btn > span");
				//selenium.click("css=span.btn.ok-btn > span");
		 
		{selenium.click("id=GenerateExamPageContainer_generateCourse");
		selenium.type("css=input.keyword-input", "liver");
		selenium.click("css=div.btn.search-btn > span");
		selenium.click("css=div.start-btn.btn > span");
		selenium.click("css=span.btn.ok-btn > span");}
		
	}

	private Object pageTitle(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {}
	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
