package AACC_Basic_Functionality;

//This login will only work when testing on staging. This will not work on live
//CHANGE FOR GIT 2

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.Selenium;

public class A_AACC_Login {
	private Selenium selenium;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*firefox", "http://aacc-staging.endpoints.roundpoint.com/");
		selenium.start();
	}

	@Test
	public void testAACC_Login() throws Exception {
		selenium.open("/login/");
		selenium.click("link=Login as a Known User");
		selenium.waitForPageToLoad("30000");
		selenium.click("xpath=(//button[@type='button'])[2]");
		selenium.waitForPageToLoad("30000");
		selenium.click("xpath=(/html/body/div/div/div/ul/li/span[@id='topNavButton_account'])");
		selenium.click("id=topNavButton_about");
		selenium.click("id=topNavButton_home");


	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}


